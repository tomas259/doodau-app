function subscribeToNotifications()
{
    if(window.plugins && window.plugins.OrtcPushPlugin){
        var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
        OrtcPushPlugin.log("Connecting");

        OrtcPushPlugin.connect({
            'appkey':'p2rG6i',
            'token':'3afxcv4ymzzsfmovdon22kmh',
            'metadata':'myMetadata',
            'url':'https://ortc-developers.realtime.co/server/ssl/2.1/',
            'projectId':'950766769041'
        }, function (){
            OrtcPushPlugin.log("Connected: ");
            var channel = document.getElementById('channel');
            OrtcPushPlugin.log("Trying to subscribe: " + channel.value);
            OrtcPushPlugin.subscribe({
                'channel':channel.value
            },  function (){
                var subscribed = document.getElementById('subscribed');
                subscribed.innerHTML = "subscribed: " + channel.value;
                OrtcPushPlugin.log("subscribed: " + channel.value);
            });
        });
    }
}

document.addEventListener("push-notification", function(notification) {
    window.plugins.OrtcPushPlugin.log(notification.payload);
    var payload = document.getElementById('payload');
    payload.innerHTML = "payload: " + notification.payload.name;
    payload.value = "payload: " + notification.payload.name;
}, false)